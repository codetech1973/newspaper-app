from django.contrib import admin

from . import models

# Register your models here.


class CommmentInline(admin.StackedInline):
    model = models.Comment


class ArticleAdmin(admin.ModelAdmin):
    inlines = [
        CommmentInline,
    ]

admin.site.register(models.Article, ArticleAdmin)
admin.site.register(models.Comment)
