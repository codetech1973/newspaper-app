from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin # new
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy

# Create your views here.

from . models import Blog


class BlogListView(LoginRequiredMixin, ListView):
    model = Blog
    template_name = 'blog-list.html'
    login_url = 'login'


class BlogDetailView(LoginRequiredMixin, DetailView):
    model = Blog
    template_name = 'blog-detail.html'
    login_url = 'login'


class BlogUpdateView(LoginRequiredMixin, UpdateView):
    model = Blog
    fields = ['title', 'body', ]
    template_name = 'blog-edit.html'
    login_url = 'login'

class BlogDeleteView(LoginRequiredMixin, DeleteView):
    model = Blog
    template_name = 'blog-delete.html'
    success_url = reverse_lazy('blog-list')
    login_url = 'login'


class BlogCreateView(LoginRequiredMixin, CreateView):
    model = Blog
    template_name = 'blog-new.html'
    fields = '__all__'
    login_url = 'login'

    def for_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)
