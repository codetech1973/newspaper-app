from django.urls import path

from . import views


urlpatterns = [

    path('', views.BlogListView.as_view(), name= 'blog-list'),

    path('<int:pk>/',
        views.BlogDetailView.as_view(), name='blog-detail'),

    path('<int:pk>/edit/',
        views.BlogUpdateView.as_view(), name='blog-edit'),

    path('<int:pk>/delete',
        views.BlogDeleteView.as_view(), name='blog-delete'),

    path('blog', views.BlogCreateView.as_view(), name='blog-new'),


]
