from django.contrib import admin

from . import models

# Register your models here.

class CommentInline(admin.TabularInline):
    model = models.Comment



class BlogAdmin(admin.ModelAdmin):
    inlines = [
    CommentInline,


    ]

admin.site.register(models.Blog, BlogAdmin)
admin.site.register(models.Comment)
